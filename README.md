<img src="https://github.com/walysonfelipe/walysonfelipe/blob/main/assets/eu.png?raw=true" min-width="250px" max-width="250px" width="250px" align="right" alt="walyson">

Meu nome é [Walyson Felipe](https://www.walyson.com.br), conheci a programação aos 14 anos através de um amigo que fazia mods para minecraft desde então venho estudando é hoje sou freelancer como Full Stack Developer.

☕ *Habilidades* : HTML, CSS, Sass, Tailwind CSS, Javascript, Php, Typescript, VenomBot, NuxtJs, GraphQl.

💼 *Ferramentas* : Figma, Visual Code, Git e GitHub.


<p align="left">
  <a href="https://www.instagram.com/walyson.assis/" alt="Instagram">
    <img src="https://img.shields.io/badge/-Instagram-02000B?style=for-the-badge&logo=Instagram&logoColor=FFD600&link=https://www.instagram.com/walyson.assis"/>
  </a>
  
  <a href="https://www.linkedin.com/in/walysonfelipee" alt="Linkedin">
    <img src="https://img.shields.io/badge/-Linkedin-02000B?style=for-the-badge&logo=Linkedin&logoColor=FFD600&link=https://www.linkedin.com/in/walysonfelipee"/>
  </a>
  
  <a href="https://api.whatsapp.com/send?phone=5514998680507&text=Ol%C3%A1%2C%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20seus%20servi%C3%A7os%3F" alt="Whatsapp">
    <img src="https://img.shields.io/badge/-Whatsapp-02000B?style=for-the-badge&logo=Whatsapp&logoColor=FFD600&link=https://api.whatsapp.com/send?phone=5514998680507&text=Ol%C3%A1%2C%20Gostaria%20de%20mais%20informa%C3%A7%C3%B5es%20sobre%20seus%20servi%C3%A7os%3F"/>
  </a>
</p>
